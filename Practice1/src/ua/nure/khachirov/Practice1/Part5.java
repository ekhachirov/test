package ua.nure.khachirov.Practice1;

public class Part5 {
	public static void main(String[] args) {
		String arrow = " ==> ";
		System.out.println("A" + arrow + str2int("A") + arrow + int2str(str2int("A")));
		System.out.println("B" + arrow + str2int("B") + arrow + int2str(str2int("B")));
		System.out.println("Z" + arrow + str2int("Z") + arrow + int2str(str2int("Z")));
		System.out.println("AA" + arrow + str2int("AA") + arrow + int2str(str2int("AA")));
		System.out.println("AZ" + arrow + str2int("AZ") + arrow + int2str(str2int("AZ")));
		System.out.println("BA" + arrow + str2int("BA") + arrow + int2str(str2int("BA")));
		System.out.println("ZZ" + arrow + str2int("ZZ") + arrow + int2str(str2int("ZZ")));
		System.out.println("AAA" + arrow + str2int("AAA") + arrow + int2str(str2int("AAA")));
	}

	public static int str2int(String number) {
		char[] charArray = number.toCharArray();
		int rezult = 0;
		int charLength = charArray.length;
		for (int i = 0; i < charArray.length; i++) {
			int curentOrderOfChar = charArray[i] - 64;
			if (charArray.length == 1) {
				rezult += curentOrderOfChar;
				return rezult;
			}

			int ctepen = (int) Math.pow(26, charLength -= 1);
			rezult += curentOrderOfChar * ctepen;
		}

		return rezult;
	}

	public static String int2str(int number) {

		StringBuilder chars = new StringBuilder();
		StringBuilder charsInReversOrder = new StringBuilder();

		int modul;
		int divident = number;
		while (divident != 0) {
			modul = divident % 26;
			if (modul == 0) {
				chars.append("Z");
				divident = (divident - 1) / 26;
			} else {
				chars.append((char) (modul + 64));
				divident = (divident - modul) / 26;
			}
		}
		for (int i = 0; i < chars.length(); i++) {
			charsInReversOrder.append(chars.charAt(chars.length() - i - 1));
		}
		return charsInReversOrder.toString();

	}

	public static String rightColumn(String number) {
		int temp = str2int(number);
		return int2str(temp + 1);
	}
}
