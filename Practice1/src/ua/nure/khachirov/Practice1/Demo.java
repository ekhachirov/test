package ua.nure.khachirov.Practice1;

public class Demo {
	public static void main(String[] args) {
		Part1.main(new String[] {});
		Part2.main(new String[] { "1", "2" });
		Part3.main(new String[] { "30000", "1701" });
		Part4.main(new String[] { "111" });
		Part5.main(new String[] { "A", "B", "Z", "AA", "AZ", "BA", "ZZ", "AAA" });
	}
}
