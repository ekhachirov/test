package ua.nure.khachirov.Practice1;

public class Part3 {
	public static void main(String[] args) {
		int firstNumber = Integer.parseInt(args[0]);
		int secondNumber = Integer.parseInt(args[1]);
		System.out.println(greatestCommonDivisor(firstNumber, secondNumber));

	}

	public static int greatestCommonDivisor(int a, int b) {
		if (b == 0) {
			return a;
		}
		int x = a % b;
		return greatestCommonDivisor(b, x);
	}
}
