package ua.nure.khachirov.Practice8;

import java.sql.SQLException;

public class Demo {
    public static void main(String[] args) throws SQLException {
DBManager dbManager=new DBManager();
        dbManager.insertUser(dbManager.createUser("petrov"));
        dbManager.insertUser(dbManager.createUser("obama"));
       dbManager.printList( dbManager.findAllUsers());
dbManager.insertGroup(dbManager.createGroup("A"));
dbManager.insertGroup(dbManager.createGroup("B"));
dbManager.printList(dbManager.findAllGroups());

dbManager.setGroupsForUser(dbManager.createUser("obama"),dbManager.createGroup("A"),dbManager.createGroup("B"));
dbManager.getUserGroups(dbManager.createUser("obama"));
dbManager.deleteGroup(dbManager.createGroupWithId(1,"A"));
dbManager.updateGroup(dbManager.createGroupWithId(2,"Updated"));
        System.out.println("afterChanging");
dbManager.printList(dbManager.findAllUsers());
        dbManager.printList(dbManager.findAllGroups());

    }
    }


