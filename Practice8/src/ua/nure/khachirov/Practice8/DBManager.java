package ua.nure.khachirov.Practice8;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DBManager {
    private final static String INSERT_USER="INSERT INTO users values(DEFAULT,?)";
    private final static String INSERT_GROUP="INSERT INTO groups values(DEFAULT,?)";
    private final static String GET_ALL_USER="SELECT * FROM users";
    private final static String GET_ALL_Group="SELECT * FROM groups";
    private final static String INSERT_GROUP_USERS="INSERT INTO users_groups values(?,?)";
    private final static String GET_USER_GROUPS="SELECT id,name FROM groups INNER JOIN users_groups WHERE user_id=? and id=group_id;";
    private final static String DELETE_GROUP="DELETE FROM groups WHERE name=?";
    private final static String UPDATE_GROUP="UPDATE groups SET name=? where id=?";
    public void insertGroup(Group group){
        Connection con= null;
        PreparedStatement st=null;
        try {
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/test","root","root");
            st=con.prepareStatement(INSERT_GROUP);
            st.setString(1,group.getName());
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println("something wrong with inserting group");
        }finally {
            try {
                st.close();
                con.close();
            } catch (SQLException e) {
                System.out.println("Can't close resourse in method insertUser");
            }
        }

    }
    public List<Group> findAllGroups(){
        List<Group> listGroup=new ArrayList<>();
        Connection  con=null;
        Statement st=null;
        ResultSet rs=null;
        try {
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/test","root","root");
            st=con.createStatement();
            rs=st.executeQuery(GET_ALL_Group);
            while (rs.next()){
                listGroup.add(createGroupWithId(rs.getInt("id"),rs.getNString("name")));
            }
        } catch (SQLException e) {
            System.out.println("something wrong with getting all groups");
        }finally {
            try {
                rs.close();
                st.close();
                con.close();
            } catch (SQLException e) {
                System.out.println("Can't close resourse in method findALLGroups");
            }

        }
        return listGroup;
    }
    public void insertUser(User user){
        Connection con= null;
        PreparedStatement st=null;
        try {
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/test","root","root");
             st=con.prepareStatement(INSERT_USER);
            st.setString(1,user.getName());
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println("something wrong with inserting user");
        }finally {
            try {
                st.close();
                con.close();
            } catch (SQLException e) {
                System.out.println("Can't close resourse in method insertUser");
            }
        }

    }
    public   <T> void printList(List<T> list){
        for (T element:list) {
            System.out.println(element);

        }
    }
 public List<User> findAllUsers(){
        List<User> userList=new ArrayList<>();
     Connection  con=null;
     Statement st=null;
     ResultSet rs=null;
     try {
          con = DriverManager.getConnection("jdbc:mysql://localhost:3306/test","root","root");
          st=con.createStatement();
         rs=st.executeQuery(GET_ALL_USER);
         while (rs.next()){
             userList.add(createUserWithId(rs.getInt("id"),rs.getNString("login")));
         }
     } catch (SQLException e) {
         System.out.println("something wrong with getting all users");
     }finally {
         try {
             rs.close();
             st.close();
             con.close();
         } catch (SQLException e) {
             System.out.println("Can't close resourse in method findALLUsers");
         }

     }


     return userList;
 }
 private boolean checkUser(Connection c,User u)  {
     PreparedStatement checkUser=null;
     ResultSet res = null;
     boolean rez=false;
     try {
         checkUser=c.prepareStatement("SELECT * FROM users WHERE login=?");
         checkUser.setString(1,u.getName());
         res=checkUser.executeQuery();
         rez= res.next()?true:false;
     } catch (SQLException e) {
         System.out.println("something wrong with checking user");
     }finally {
         try {
             res.close();
             checkUser.close();
         } catch (SQLException e) {
             System.out.println("something wrong with closing resource in checkUser");
         }

     }
     return rez;

 }
 private int getUserId(Connection c,User u)  {
     PreparedStatement checkUser=null;
     ResultSet resultSetForCheckUser=null;
     int rez=0;
     try {
         checkUser=c.prepareStatement("SELECT id FROM users WHERE login=?");
         checkUser.setString(1,u.getName());
         resultSetForCheckUser=checkUser.executeQuery();
         resultSetForCheckUser.next();
        rez= resultSetForCheckUser.getInt(1);
     } catch (SQLException e) {
         e.printStackTrace();
         System.out.println("cant get user ID");
     }

     return rez;
 }
private boolean checkGroup(Connection c,Group g){
    PreparedStatement checkGroup=null;
    ResultSet res = null;
    boolean rez=false;
    try {
        checkGroup=c.prepareStatement("SELECT * FROM groups WHERE name=?");
        checkGroup.setString(1,g.getName());
        res=checkGroup.executeQuery();
        rez= res.next()?true:false;
    } catch (SQLException e) {
        System.out.println("something wrong with checking group");
    }finally {
        try {
            res.close();
            checkGroup.close();
        } catch (SQLException e) {
            System.out.println("something wrong with closing resource in checkGroup");
        }

    }
    return rez;

}
private int getGroupId(Connection c,Group g){
    PreparedStatement checkGroup=null;
    ResultSet resultSetForCheckGroup=null;
    int rez=0;
    try {
        checkGroup=c.prepareStatement("SELECT id FROM groups WHERE name=?");
        checkGroup.setString(1,g.getName());
        resultSetForCheckGroup=checkGroup.executeQuery();
        resultSetForCheckGroup.next();
        rez= resultSetForCheckGroup.getInt(1);
    } catch (SQLException e) {
        e.printStackTrace();
        System.out.println("cant get group ID");
    }

    return rez;

}
 public void setGroupsForUser(User user ,Group ...  group){
     Connection connectionUtil=null;
     Connection mainConnection=null;
     PreparedStatement mainPreparedStatement=null;
     int userKey;
     int[]groupsKeys=new int[group.length];

     try {
         connectionUtil = DriverManager.getConnection("jdbc:mysql://localhost:3306/test",
                 "root","root");
         if(checkUser(connectionUtil,user)){
             userKey=getUserId(connectionUtil,user);
         }else {
             insertUser(user);
             userKey=getUserId(connectionUtil,user);
         }
         int i=0;
         for(Group g:group){
             if(checkGroup(connectionUtil,g)){
                 groupsKeys[i]=getGroupId(connectionUtil,group[i]);

             }else {
                 insertGroup(group[i]);
                 groupsKeys[i]=getGroupId(connectionUtil,group[i]);
             }
             i++;
         }
         mainConnection=DriverManager.getConnection("jdbc:mysql://localhost:3306/test",
                 "root","root");
         mainConnection.setAutoCommit(false);
         mainConnection.setTransactionIsolation(4);
         mainPreparedStatement=mainConnection.prepareStatement(INSERT_GROUP_USERS);
         for(int groupKey:groupsKeys){
             mainPreparedStatement.setInt(1,userKey);
             mainPreparedStatement.setInt(2,groupKey);
             mainPreparedStatement.executeUpdate();
         }
mainConnection.commit();




     } catch (SQLException e) {

         try {
             mainConnection.rollback();
             connectionUtil.close();
             mainConnection.close();
         } catch (SQLException e1) {
             System.out.println("Cant close connections in setGroupsForUser() or cant rollback");
         }
     }


 }
public  List<Group> getUserGroups(User user){
     List<Group>groupList=new ArrayList<>();
    Connection connection=null;
    PreparedStatement preparedStatement=null;
    ResultSet rs=null;
     try {
         connection=DriverManager.getConnection("jdbc:mysql://localhost:3306/test","root","root");

         preparedStatement= connection.prepareStatement(GET_USER_GROUPS);
         preparedStatement.setInt(1,getUserId(connection,user));
         rs=preparedStatement.executeQuery();
         while(rs.next()){
             groupList.add(createGroupWithId(rs.getInt("id"),rs.getString("name")));
         }


    } catch (SQLException e) {
         System.out.println("can not obtain user's groups");
    }finally {
         try {
             connection.close();
         } catch (SQLException e) {
             System.out.println("can not close connection in getUserGroups");
         }
     }
    return  groupList;
}
public void deleteGroup(Group group){

    Connection connection=null;
    PreparedStatement statement;
    try{
        connection=DriverManager.getConnection("jdbc:mysql://localhost:3306/test","root","root");
statement=connection.prepareStatement(DELETE_GROUP);
statement.setString(1,group.getName());
statement.execute();
    }catch (SQLException e){
        System.out.println("Cant delete group");
    }finally {
        try {
            connection.close();
        } catch (SQLException e) {
            System.out.println("cant close connection in deleteGroup()");
        }
    }
}
public void updateGroup(Group group){
    Connection connection=null;
    PreparedStatement prepareDStatement;
    try{
        connection=DriverManager.getConnection("jdbc:mysql://localhost:3306/test","root","root");
        prepareDStatement=connection.prepareStatement(UPDATE_GROUP);
        prepareDStatement.setString(1,group.getName());
        prepareDStatement.setInt(2,group.getId());
        prepareDStatement.execute();
    }catch (SQLException e){
        System.out.println("cant update group");
    }finally {
        try {
            connection.close();
        } catch (SQLException e) {
            System.out.println("cant close connectin in updateGroup()");
        }

    }
}
    public User createUser(String name){
         return new User(name);
     }
    public User createUserWithId(int id,String name){
        return new User(id,name);
    }
    public Group createGroup(String name){
        return new Group(name);
    }
    public Group createGroupWithId(int id,String name){
        return new Group(id,name);
    }



}
