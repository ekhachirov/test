

 drop database test ;
 create database test;
 use test;
 create table users(id int auto_increment,login varchar(20) not null,primary key(id),unique(login));
 create table groups(id int auto_increment,name varchar(20) not null,primary key(id),unique(name));
 create table users_groups(user_id int ,group_id int,foreign key(user_id) references users(id),foreign key(group_id) references groups(id) ON DELETE CASCADE);