package ua.nure.khachirov.Practice9;

import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/PartServlet4")
public class PartServlet4 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
    public PartServlet4() {
       super();
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		ServletContext servletContext=getServletContext();
		Object names=servletContext.getAttribute("names");
		request.setAttribute("names", names);
		request.getRequestDispatcher("out.jsp").forward(request, response);
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doGet(request, response);
	}

}
