package ua.nure.khachirov.Practice9;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.websocket.Session;


@WebServlet("/PartServlet42")
public class PartServlet42 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
   
    public PartServlet42() {
        super();
        
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	
		
		
		List<String>selected=(List<String>) getServletContext().getAttribute("selected");
		Map<String, List<String>>names_categories=(Map<String, List<String>>) getServletContext().getAttribute("names_categories");
		selected.add(request.getParameter("select"));
	 
	 

	 
	 request.getSession().setAttribute("name",request.getParameter("name"));
 boolean flag=false;
 for(String s:names_categories.keySet()){
	 if(names_categories.get(s).contains(request.getSession().getAttribute("name"))){
		 flag=true;
	 }
 }
 if(!flag){
	 List<String>temp=names_categories.get(request.getParameter("select"));
	 temp.add(request.getParameter("name"));
		 request.getRequestDispatcher("/final.jsp").forward(request, response);
 }else{
	 request.getRequestDispatcher("/error4.html").forward(request, response);
 }
		
	}

	
	 
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doGet(request, response);
	}

}
