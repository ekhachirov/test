package ua.nure.khachirov.Practice9;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/culc")
public class MyServlet1 extends HttpServlet {
	private static final long serialVersionUID = 1L;
public static int calculate(String paramA,String paramB,String op){
	int param1=Integer.parseInt(paramA);
	int param2=Integer.parseInt(paramB);
	switch(op){
	case "+":return param1+param2;
	case "-":return param1-param2;
	default :return 0;
	}
}
   
    public MyServlet1() {
        
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int rez=calculate(request.getParameter("x"), request.getParameter("y"), request.getParameter("sign"));
		response.getWriter().write("<html><body>"+"rez="+rez+"<br><a href='http://localhost:8080/Practice9/Part1.html'>return</a>"+"<body></html>");
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doGet(request, response);
	}

}
