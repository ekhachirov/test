package ua.nure.khachirov.Practice9;



import java.util.*; import javax.servlet.*; import javax.servlet.annotation.WebListener;
 
@WebListener public class MyListener implements ServletContextListener {  
	public void contextInitialized(ServletContextEvent sce) {     
		ServletContext sc = sce.getServletContext();    
		List<String> names =new ArrayList<>();
		List<String> selected =new ArrayList<>();
		Map<String, List<String>>names_categories=new HashMap<>();
		for(String s:sc.getInitParameter("names").split(" ")){
			names.add(s);
			names_categories.put(s, new ArrayList<>());
		}
		
		System.out.println("names ==> " + names);
		sc.setAttribute("names_categories", names_categories);
		sc.setAttribute("names", names);
		sc.setAttribute("selected", selected);
		}        
	public void contextDestroyed(ServletContextEvent sce) {} }

