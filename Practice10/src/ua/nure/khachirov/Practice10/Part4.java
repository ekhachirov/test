package ua.nure.khachirov.Practice10;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Part4
 */
@WebServlet("/PartServlet4")
public class Part4 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Part4() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @throws IOException 
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	
    	String SQL_CHECK_LOGIN_AND_PASSWOD="SELECT * FROM USERS WHERE login=? and password=?";
    	DatabaseManager databaseManager=DatabaseManager.getInstance();  
	 Connection connection=null;
	 PreparedStatement preparedStatement=null;
	 ResultSet resultSet=null;
	 try {
		connection=databaseManager.getConnection();
		preparedStatement=connection.prepareStatement(SQL_CHECK_LOGIN_AND_PASSWOD);
		
		preparedStatement.setString(1, request.getParameter("login"));
		preparedStatement.setString(2, request.getParameter("password"));
		resultSet=preparedStatement.executeQuery();
		
		if(resultSet.next()){
			String role=null;
			if((resultSet.getInt(4))==1){
				role="admin";
			}else{
				role="client";
			}
			request.getSession().setAttribute("curLogin", request.getParameter("login"));
			request.getSession().setAttribute("curRole", role);
			response.sendRedirect("Part3.jsp");
		}else {
			response.sendRedirect("Part4.jsp");
		}

	} catch (SQLException e) {
		// TODO Auto-generated catch block
	e.printStackTrace();
	}
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
	}

}
