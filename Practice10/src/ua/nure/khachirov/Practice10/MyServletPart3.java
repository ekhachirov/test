package ua.nure.khachirov.Practice10;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/MyServletPart3")
public class MyServletPart3 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
    public MyServletPart3() {
        super();
       
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
	}

	
	
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	 if(request.getParameter("remove")!=null){
			List<String>list =(List<String>)request.getSession().getAttribute("names");
list.clear();
	 }
		if(request.getSession().isNew()){
			request.getSession().setAttribute("names", new ArrayList<String>());	
		}
			List<String>list =(List<String>)request.getSession().getAttribute("names");
			
			list.add(request.getParameter("name"));
	
		
		response.sendRedirect("Part3.jsp");
		
	}

}
