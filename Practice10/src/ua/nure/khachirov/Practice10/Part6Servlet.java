package ua.nure.khachirov.Practice10;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Part6Servlet
 */
@WebServlet("/Part6Servlet")
public class Part6Servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Part6Servlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String SQL_RENAME="update users set name=? where role_id=?";
		DatabaseManager databaseManager=DatabaseManager.getInstance();  
		 Connection connection=null;
		 PreparedStatement preparedStatement=null;
		 try {
			connection=databaseManager.getConnection();
			preparedStatement=connection.prepareStatement(SQL_RENAME);
			
			preparedStatement.setString(1, request.getParameter("rename"));
			 
			preparedStatement.setString(2,(String) request.getSession().getAttribute("curLogin"));
			
		preparedStatement.executeUpdate();
			response.sendRedirect("Part6.jsp");
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

}
