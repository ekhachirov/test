package ua.nure.khachirov.Practice10;

import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;



public class DatabaseManager {
	private static DatabaseManager instance=null;
	private DataSource dataSource=null;
	public DatabaseManager() {
		try {
			Context initContext = new InitialContext();
			Context envContext = (Context) initContext.lookup("java:/comp/env");
			dataSource = (DataSource) envContext.lookup("jdbc/practice10");
		} catch (NamingException e) {
			// loger here
		
		}
	}
	public Connection getConnection() throws SQLException{
		return dataSource.getConnection();
	}
	public static synchronized DatabaseManager getInstance(){
		if(instance==null){
			instance=new DatabaseManager();
		}
		return instance;
	}
}
