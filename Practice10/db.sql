DROP DATABASE IF EXISTS practice10;
create database practice10;

create table roles( id int primary key auto_increment,name varchar(20) unique);
create table users(login varchar(20) not null unique, name varchar(20),password varchar(20) unique not null ,role_id int , FOREIGN KEY (role_id) references roles(id) );

insert into roles values(default,"admin");
insert into roles values(default,"user");
insert into users values("login1","dpfsdf","password1",1);
insert into users values("login2","name","password2",1);
insert into users values("���","nsdfsdfsdfe","password3",2);