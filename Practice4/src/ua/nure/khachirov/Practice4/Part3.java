package ua.nure.khachirov.Practice4;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Part3 {
	public static final Map<String, String> patterns = new HashMap<>();

	public static void main(String[] args) throws IOException {

		patterns.put("double", "( |\\A)(\\d*\\.\\d*)( |\\Z)");
		patterns.put("String", "()((?U)\\p{Alpha}{2,})()");
		patterns.put("char", "()(\\b(?U)\\p{Alpha}\\b)()");
		patterns.put("int", "( |\\A)(\\d+)( |\\Z)");
		Scanner sc = new Scanner(System.in,"cp1251");
		Pattern p;
		Matcher m;
		StringBuilder rez = new StringBuilder();
		while (sc.hasNext()) {

			String pattern = patterns.get(sc.nextLine());
			if (pattern == null) {
				continue;
			}

			p = Pattern.compile(pattern);
			m = p.matcher(Part1.readFromFile("part3.txt"));
			while (m.find()) {
				rez.append(m.group(2)).append(" ");
			}
			System.out.println(rez);
			rez = new StringBuilder();

		}
		sc.close();

	}
}
