package ua.nure.khachirov.Practice4;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Part1 {

	public static void main(String[] args) {

		printFormated("part1.txt");

	}

	static public void printFormated(String fileName) {
		StringBuilder sb = new StringBuilder(readFromFile(fileName));
		Pattern p = Pattern.compile("(?U)\\b\\w{4,}\\b");
		Matcher m = p.matcher(sb);
		StringBuffer rez = new StringBuffer();
		while (m.find()) {
			m.appendReplacement(rez, m.group().toUpperCase());
		}
		m.appendTail(rez);
		System.out.println(rez);
	}

	public static String readFromFile(String fileName) {
		final String encoding = "Cp1251";
		String res = null;
		try {
			byte[] bytes = Files.readAllBytes(Paths.get(fileName));
			res = new String(bytes,encoding);
		} catch (IOException ex) {
			System.out.println("Can not read file ");
		}
		return res;

	}

}
