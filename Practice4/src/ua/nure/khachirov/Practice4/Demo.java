package ua.nure.khachirov.Practice4;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

public class Demo {
	public static final InputStream STD_IN = System.in;
	public static final String ENCODING = "cp1251";

	public static void main(String[] args) throws IOException {
		System.out.println("========================Part1");
		Part1.main(args);
		System.out.println("========================Part2");
		Part2.main(args);
		System.out.println("========================Part3");
		System.setIn(new ByteArrayInputStream("char\nString\nint\ndouble".getBytes(ENCODING)));
		Part3.main(args);
		System.out.println("========================Part4");
		Part4.main(args);
		System.out.println("========================Part5");
		System.setIn(new ByteArrayInputStream("table ru\ntable en\napple ru".getBytes(ENCODING)));
		Part5.main(args);
		System.setIn(STD_IN);

	}
}
