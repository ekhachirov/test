package ua.nure.khachirov.Practice4;

import java.io.*;
import java.util.Random;

public class Part2 {

	public static void createFile(String path) {

		try {
			File file = new File(path);
			file.createNewFile();
		} catch (IOException e) {
			System.out.println("Something wrong whith creating File");
		}
	}

	public static String creatingRandomNumbers() {
		Random r=new Random();
		
		StringBuilder inFile = new StringBuilder();

		for (int i = 0; i < 10; i++) {
			inFile.append(r.nextInt(50)+" ");
		}
		return inFile.toString();
	}

	public static void writeToFile(String fileName, String strToWrite) {

		try {
			FileWriter fw1 = new FileWriter(fileName);
			BufferedWriter bw1 = new BufferedWriter(fw1);
			bw1.write(strToWrite);
			bw1.flush();
			bw1.close();
		} catch (IOException e) {
			System.out.println("Something wrong whith writing");
		}

	}

	public static String sorting(String fileName) {
		String[] numbersStr = Part1.readFromFile(fileName).split(" ");
		int[] numbersInt = new int[numbersStr.length ];
		for (int i = 0; i < numbersStr.length ; i++) {
			numbersInt[i] = Integer.valueOf(numbersStr[i]);
		}

		StringBuilder rez = new StringBuilder();

		int tempInt;
		for (int i = 0; i < numbersInt.length; i++) {
			for (int j = 1; j < numbersInt.length; j++) {
				if (numbersInt[j] < numbersInt[j - 1]) {
					tempInt = numbersInt[j];
					numbersInt[j] = numbersInt[j - 1];
					numbersInt[j - 1] = tempInt;
				}
			}
		}

		for (int i = 0; i < numbersInt.length; i++) {
			rez.append(String.valueOf(numbersInt[i])).append(" ");
		}
		return rez.toString();
	}

	public static void printToConsole(String src, String dest) {
		StringBuilder sbSrc = new StringBuilder(Part1.readFromFile(src));
		StringBuilder sbDest = new StringBuilder(Part1.readFromFile(dest));
		sbSrc.deleteCharAt(sbSrc.lastIndexOf(" "));
		sbDest.deleteCharAt(sbDest.lastIndexOf(" "));
		System.out.println("input ==> " + sbSrc);
		System.out.println("output ==> " + sbDest);

	}

	public static void main(String[] args) throws IOException {
		String srcFile = "./part2.txt";
		String destFile = "./part2_sorted.txt";
		createFile(srcFile);
		writeToFile(srcFile, creatingRandomNumbers());
		createFile(destFile);
		writeToFile(destFile, sorting(srcFile));
		printToConsole(srcFile, destFile);
	}

}
