package ua.nure.khachirov.Practice4;


import java.util.Arrays;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Part4 implements Iterable<String>  {
	public static void main(String[]args){
		Part4 p=new Part4();
		Iterator<String> i=p.iterator();
		while(i.hasNext()){
			System.out.println(i.next().replaceAll(System.lineSeparator(), ""));
			}
			
		}
	
	StringBuilder allSenteces=new StringBuilder(Part1.readFromFile("part4.txt"));
	

	@Override
	public Iterator<String> iterator() {
		// TODO Auto-generated method stub
		return new MyIterator();
	}

	class MyIterator implements Iterator<String>{
	Pattern p=Pattern.compile("(?s)\\p{javaUpperCase}.+?\\.");	;
	Matcher m=p.matcher(allSenteces);
String[]storagetemp=new String[100];
String []storage;
{int c=0;
	while(m.find()){
	storagetemp[c]=m.group();
	c++;
	
}
	storage=Arrays.copyOf(storagetemp, c);
	}

protected int cursor;
protected int mod;
protected int lastObjectIndex;
protected int operationsMod;
int size=storage.length;
public boolean hasNext() {

	return cursor != size;
}
public void remove() {
	throw new UnsupportedOperationException();
}
public String next() {
	
	String o;
	if (hasNext()) {
		o = storage[cursor];
		lastObjectIndex = cursor;
		cursor++;
		mod++;
		if (operationsMod > 0) {
			operationsMod = 0;
		}
	} else {
		throw new NoSuchElementException();
	}
	return o;
}



				
	}
	

}
