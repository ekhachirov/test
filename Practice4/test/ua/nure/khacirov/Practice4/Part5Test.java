package ua.nure.khacirov.Practice4;

import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ua.nure.khachirov.Practice4.Part5;

public class Part5Test {
	@Before
	public void setOut() {
		PrintStream p = new PrintStream(out);
		System.setOut(p);
	}

	@After
	public void delOut() {
		System.setOut(System.out);
	}
	ByteArrayOutputStream out = new ByteArrayOutputStream();
	@Test
	public void test() throws UnsupportedEncodingException {
		String s="����"+System.lineSeparator()+"table"+System.lineSeparator()+"������";

		final String ENCODING = "cp1251";
		System.setIn(new ByteArrayInputStream("table ru\ntable en\napple ru".getBytes(ENCODING)));
		Part5.main(new String[]{});
		assertEquals(s, out.toString().trim());
	}

}
