package ua.nure.khacirov.Practice4;

import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ua.nure.khachirov.Practice4.Part1;
import ua.nure.khachirov.Practice4.Part2;

public class Part2Test {
	ByteArrayOutputStream out = new ByteArrayOutputStream();
	@Test
	public void CreateFiletest() {
		Part2.createFile("./bla.txt");
		File f=new File("./bla.txt");
		if(f.exists()){
			assertTrue(true);
			f.delete();
		}else fail();
	}
	@Test
	public void randomNumbersTest(){
		assertNotNull(Part2.creatingRandomNumbers());
	}
	@Test
	public void writeToFileTest(){
		Part2.writeToFile("./test.txt","bla");
		assertEquals("bla",Part1.readFromFile("./test.txt").trim());
		File f=new File("./test.txt");
		f.delete();
	}
	@Test
	public void sorting(){
		String expected="1 2 3 5 7 ";
		assertEquals(expected,Part2.sorting("./TestSorting.txt"));
	}
	@Before
	public void setOut() {
		PrintStream p = new PrintStream(out);
		System.setOut(p);
	}

	@After
	public void delOut() {
		System.setOut(System.out);
	}
	@Test
	public void testConsole(){
		
Part2.printToConsole("./part2.txt","./part2_sorted.txt");
assertEquals(out.toString().trim(), out.toString().trim());
	}
	public void testMain() throws IOException{
		Part2.main(new String[0]);
 assertTrue(true);
	}

}
