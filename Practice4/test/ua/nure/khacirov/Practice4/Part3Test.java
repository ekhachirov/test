package ua.nure.khacirov.Practice4;

import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;


import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ua.nure.khachirov.Practice4.Part3;

public class Part3Test {
	@Before
	public void setOut() {
		PrintStream p = new PrintStream(out);
		System.setOut(p);
	}

	@After
	public void delOut() {
		System.setOut(System.out);
	}

	ByteArrayOutputStream out = new ByteArrayOutputStream();
	private static final String ENCODING = "cp1251";

	@Test
	public void test() throws IOException {
		String s="a � � "+System.lineSeparator()+"bcd ���� "+System.lineSeparator()+"432 89 "+System.lineSeparator()+"43.43 .98";
	
		System.setIn(new ByteArrayInputStream("char\nString\nint\ndouble".getBytes(ENCODING)));
		Part3.main(new String[]{});
		assertEquals(s, out.toString().trim());
	

}
	}
