package ua.nure.khacirov.Practice4;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ Part1Test.class, Part2Test.class, Part3Test.class, Part4Test.class, Part5Test.class })


public class AllTests { 



	@Test

	public  void Test1() throws IOException  {

		

		Part1Test p = new Part1Test();

		p.setOut();
		p.printFormatedTest();
		p.delOut();

	}

	

	@Test

	public  void Test2() throws IOException  { 

		

		Part2Test p2 = new Part2Test();

		p2.setOut();
		p2.CreateFiletest();
		p2.randomNumbersTest();
		p2.sorting();
		p2.testConsole();
		p2.testMain();
		p2.delOut();

	}

	

	@Test

	public  void Test3() throws IOException  {

			

		Part3Test p3 = new Part3Test();

		p3.setOut();

	p3.test();
		p3.delOut();; 

	}

	

	@Test(expected=UnsupportedOperationException.class)

	public  void Test4() throws IOException  {

			

		Part4Test p4 = new Part4Test();

		p4.test(); 
		p4.testMain();

		p4.testRemove();  

	}

	

	@Test

	public  void Test5() throws UnsupportedEncodingException  {

		

		Part5Test p5 = new Part5Test();

		p5.test();

		

	}

}
