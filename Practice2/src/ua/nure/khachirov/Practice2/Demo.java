package ua.nure.khachirov.Practice2;

import java.util.Iterator;

/**
 * Created by Ed on 05.07.2017.
 */
public class Demo {
	public static void main(String[] args) {
		MyListImpl m = new MyListImpl();
		m.add("1");
		m.add("2");
		m.add("3");
		m.add("4");
		m.add("5");
		MyList m2 = new MyListImpl();
		m2.add("1");
		m2.add("2");
		m2.add("3");
		m2.add("11");
		System.out.println("FirstTask");
		System.out.println();
		System.out.println(m);
		System.out.println(m.contains("2"));
		System.out.println(m.containsAll(m2));
		m2.clear();
		System.out.println(m2);
		m.remove("1");
		System.out.println(m);
		System.out.println();
		System.out.println("SecondTask");
		System.out.println();
		for (Object o : m) {
			System.out.println(o);
		}
		System.out.println("~~~~~~~~~~~~~~~");
		Iterator<Object> it = m.iterator();
		while (it.hasNext()) {
			System.out.println(it.next());
		}
		System.out.println();
		System.out.println("ThirdTask");
		System.out.println();
		ListIterator i = m.listIterator();
		for (Object o : m) {
			System.out.println(o);
		}
		i.next();
		i.next();
		i.previous();
		i.remove();
		i.remove();
		i.next();
		i.set(11);
		System.out.println("~~~~~~~~~~~~~~~");
		for (Object o : m) {
			System.out.println(o);
		}

	}
}
