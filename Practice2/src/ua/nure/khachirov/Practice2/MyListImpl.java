package ua.nure.khachirov.Practice2;

import java.util.Arrays;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class MyListImpl implements MyList, ListIterable {

	private int size;
	private Object[] storage;
	private int defaultLength = 15;

	public MyListImpl() {
		storage = new Object[defaultLength];
	}

	@Override
	public void add(Object e) {
		if (size + 1 < storage.length) {
			storage[size++] = e;
		} else {
			grow(storage.length);
			storage[size++] = e;

		}
	}

	@Override
	public void clear() {
		storage = new Object[storage.length];
		size = 0;
	}

	@Override
	public boolean remove(Object o) {
		boolean rez = false;
		if (o == null) {
			for (int i = 0; i < size; i++) {
				if (storage[i] == null) {

					System.arraycopy(storage, i + 1, storage, i, size - i - 1);
					size--;
				}
			}
			return true;
		} else {
			for (int i = 0; i < size; i++) {
				if (storage[i].equals(o)) {
					System.arraycopy(storage, i + 1, storage, i, size - i - 1);
					storage[size - 1] = null;
					size--;
				}
			}
			rez = true;
		}

		return rez;
	}

	@Override
	public Object[] toArray() {
		Object[] returnedArray = new Object[size];
		System.arraycopy(storage, 0, returnedArray, 0, size);
		return returnedArray;
	}

	@Override
	public String toString() {
		StringBuilder rezult = new StringBuilder();
		for (int i = 0; i < toArray().length; i++) {
			if (i == toArray().length - 1) {
				rezult.append(toArray()[i].toString());
			} else {
				rezult.append(toArray()[i].toString()).append(", ");
			}
		}
		return "[" + rezult + "]";
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public boolean contains(Object o) {
		if (o == null) {
			for (int i = 0; i < size; i++) {
				if (storage[i] == null) {
					return true;
				}
			}
		} else {
			for (int i = 0; i < size; i++) {
				if (storage[i].equals(o)) {
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public boolean containsAll(MyList c) {
		int count = 0;
		for (int j = 0; j < c.size(); j++) {
			if (this.contains(c.toArray()[j])) {
				count++;
			}
		}

		return count == c.size();
	}

	private void grow(int oldLength) {
		int newCapacity = oldLength + (oldLength >> 1);
		storage = Arrays.copyOf(storage, newCapacity);

	}

	@Override
	public Iterator<Object> iterator() {
		return new IteratorImpl();

	}

	@Override
	public ListIterator listIterator() {
		return new ListIteratorImpl();
	}

	private class IteratorImpl implements Iterator<Object> {
		protected int cursor;
		protected int mod;
		protected int lastObjectIndex;
		protected int operationsMod;

		public boolean hasNext() {

			return cursor != size;
		}

		public Object next() {
			Object o;
			if (hasNext()) {
				o = storage[cursor];
				lastObjectIndex = cursor;
				cursor++;
				mod++;
				if (operationsMod > 0) {
					operationsMod = 0;
				}
			} else {
				throw new NoSuchElementException();
			}
			return o;
		}

		public void remove() {
			if (mod != 0 & operationsMod == 0) {
				MyListImpl.this.remove(storage[lastObjectIndex]);
				cursor = lastObjectIndex;
				lastObjectIndex = 0;
				mod = 0;
				operationsMod++;

			} else {
				throw new IllegalStateException();
			}
		}

	}

	private class ListIteratorImpl extends IteratorImpl implements ListIterator {

		@Override
		public boolean hasPrevious() {

			return cursor != 0;
		}

		@Override
		public Object previous() {
			Object o;
			if (hasPrevious()) {
				o = storage[cursor - 1];
				lastObjectIndex = cursor - 1;
				cursor--;
				mod++;

				if (operationsMod > 0) {
					operationsMod = 0;
				}

			} else {
				throw new NoSuchElementException();
			}
			return o;
		}

		@Override
		public void set(Object e) {
			if ((mod != 0 | mod != 0) & operationsMod == 0) {
				storage[lastObjectIndex] = e;
				operationsMod++;
			} else {
				throw new IllegalStateException();
			}
		}

		public void remove() {
			if ((mod != 0 | mod != 0) & operationsMod == 0) {
				MyListImpl.this.remove(storage[lastObjectIndex]);
				cursor = lastObjectIndex;
				lastObjectIndex = 0;

				mod = 0;
				operationsMod++;

			} else {
				throw new IllegalStateException();
			}
		}
	}
}
