package ua.nure.khachirov.Practice2;

import java.util.Iterator;

/**
 * Created by Ed on 06.07.2017.
 */
public interface ListIterator extends Iterator<Object> {
	boolean hasPrevious();

	Object previous();

	void set(Object e);

	void remove();
}
