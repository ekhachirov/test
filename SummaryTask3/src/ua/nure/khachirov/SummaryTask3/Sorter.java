package ua.nure.khachirov.SummaryTask3;

import ua.nure.khachirov.SummaryTask3.entity.Gem;

import java.util.Comparator;

public class Sorter {
    public static final Comparator<Gem> SORT_GEM_BY_VALUE = new Comparator<Gem>(){
        @Override
        public int compare(Gem o1, Gem o2) {
            return o1.value-o2.value;
            
        }
    };
    public static final Comparator<Gem> SORT_GEM_BY_PRECIOUNSNESS = new Comparator<Gem>(){
        @Override
        public int compare(Gem o1, Gem o2) {
            return Boolean.compare(o1.isPreciousness(),o2.isPreciousness());
        }
    };
    public static final Comparator<Gem> SORT_GEM_BY_TRANSPARENCY = new Comparator<Gem>(){
        @Override
        public int compare(Gem o1, Gem o2) {
            return o1.getVisualParam().getTransparency()-o2.getVisualParam().getTransparency();
        }
    };

}
