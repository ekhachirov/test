package ua.nure.khachirov.SummaryTask3;

import org.xml.sax.SAXException;
import ua.nure.khachirov.SummaryTask3.Controllers.DomController;
import ua.nure.khachirov.SummaryTask3.Controllers.SaxController;
import ua.nure.khachirov.SummaryTask3.Controllers.StaxController;
import ua.nure.khachirov.SummaryTask3.entity.Gem;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import javax.xml.transform.TransformerException;
import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;

public class Main {
    public static void main(String[] args) throws IOException, SAXException, ParserConfigurationException, TransformerException, XMLStreamException {
        DomController domController=new DomController(args[0]);
        domController.parse();
        Comparator<Gem>c1=Sorter.SORT_GEM_BY_VALUE;
        Collections.sort(domController.getGemsFromDomController().getGems(),c1);
        domController.saveToXML(domController.getGemsFromDomController(),"output.dom.xml");

        SaxController saxController=new SaxController(args[0]);
        saxController.parse();
        Comparator<Gem>c2=Sorter.SORT_GEM_BY_TRANSPARENCY;
        Collections.sort(saxController.getGemsList(),c2);
        domController.saveToXML(saxController.getGems(),"output.sax.xml");


        StaxController  staxController=new StaxController(args[0]);
        staxController.parse();
        Comparator<Gem>c3=Sorter.SORT_GEM_BY_PRECIOUNSNESS;
        Collections.sort(staxController.getGems().getGems(),c3);
        domController.saveToXML(staxController.getGems(),"output.stax.xml");
    }
}
