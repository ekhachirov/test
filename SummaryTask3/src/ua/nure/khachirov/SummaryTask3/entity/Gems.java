package ua.nure.khachirov.SummaryTask3.entity;

import java.util.ArrayList;
import java.util.List;

public class Gems {
    private List<Gem>gems=new ArrayList<>();

    public void setGems(List<Gem> gems) {
		this.gems = gems;
	}
	public Gems() {

    }
    public List<Gem> getGems(){
        return gems;
    }
    public void putGem(Gem g){
        gems.add(g);
    }
}
