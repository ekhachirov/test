package ua.nure.khachirov.SummaryTask3.entity;

public class Gem {
    private String name;
    private  boolean preciousness;
    private String origin;
    private VisualParam visualParam;
    public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isPreciousness() {
		return preciousness;
	}

	public void setPreciousness(boolean preciousness) {
		this.preciousness = preciousness;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public VisualParam getVisualParam() {
		return visualParam;
	}

	public void setVisualParam(VisualParam visualParam) {
		this.visualParam = visualParam;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public int value;

    @Override
    public String toString() {
        return "Gem{" +
                "name='" + name + '\'' +
                ", preciousness=" + preciousness +
                ", origin='" + origin + '\'' +
                ", visualParam=" + visualParam +
                ", value=" + value +
                '}';
    }
}
