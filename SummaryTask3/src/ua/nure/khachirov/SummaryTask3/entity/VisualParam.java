package ua.nure.khachirov.SummaryTask3.entity;

public class VisualParam {
    private String color;
    private int transparency;
    private int cuttingmethod;

    public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public int getTransparency() {
		return transparency;
	}

	public void setTransparency(int transparency) {
		this.transparency = transparency;
	}

	public int getCuttingmethod() {
		return cuttingmethod;
	}

	public void setCuttingmethod(int cuttingmethod) {
		this.cuttingmethod = cuttingmethod;
	}

	public VisualParam() {
    }
}
