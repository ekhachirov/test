package ua.nure.khachirov.SummaryTask3.Controllers;

import org.xml.sax.SAXException;
import ua.nure.khachirov.SummaryTask3.entity.Gem;
import ua.nure.khachirov.SummaryTask3.entity.Gems;
import ua.nure.khachirov.SummaryTask3.entity.VisualParam;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import javax.xml.transform.stream.StreamSource;
import java.io.IOException;

public class StaxController {
    private String xmlFileName;

    public Gems getGems() {
        return gems;
    }

    
    private Gems gems;

    public StaxController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }
    public void parse() throws ParserConfigurationException, SAXException,
            IOException, XMLStreamException {

        Gem gem=null;
        VisualParam vp=null;
        String currentElement = null;
        XMLInputFactory factory = XMLInputFactory.newInstance();

        factory.setProperty(XMLInputFactory.IS_NAMESPACE_AWARE, true);

        XMLEventReader reader = factory.createXMLEventReader(
                new StreamSource(xmlFileName));
        while (reader.hasNext()) {
            XMLEvent event = reader.nextEvent();

            if (event.isCharacters() && event.asCharacters().isWhiteSpace()) {
                continue;
            }
            if (event.isStartElement()) {
                StartElement startElement = event.asStartElement();
                currentElement = startElement.getName().getLocalPart();
                if(currentElement.equals("Gems")){
                    gems=new Gems();
                    continue;
                }
                if(currentElement.equals("Gem")){
                    gem=new Gem();
                    continue;
                }
                if(currentElement.equals("VisualParameters")){
                    vp=new VisualParam();
                    continue;
                }

            }
            if (event.isCharacters()) {
                Characters characters = event.asCharacters();
                if (characters.getData().isEmpty()) {
                    continue;
                }
                if(currentElement.equals("Name")){
                    gem.setName(characters.getData());
                    continue;
                }
                if(currentElement.equals("Preciousness")){
                    gem.setPreciousness(Boolean.valueOf(characters.getData()));
                    continue;
                } if(currentElement.equals("Origin")){
                    gem.setOrigin(characters.getData());
                    continue;
                } if(currentElement.equals("Color")){
                    vp.setColor(characters.getData());
                    continue;
                }
                if(currentElement.equals("Transparency")){
                    vp.setTransparency(Integer.valueOf(characters.getData()));
                    continue;
                }
                if(currentElement.equals("Cuttingmethod")){
                    vp.setCuttingmethod(Integer.valueOf(characters.getData()));
                    continue;
                }
                if(currentElement.equals("Value")){
                    gem.value=Integer.valueOf(characters.getData());
                    continue;
                }
            }
            if (event.isEndElement()) {
                EndElement endElement = event.asEndElement();
                String localName = endElement.getName().getLocalPart();
                if(localName.equals("VisualParameters")){
                    gem.setVisualParam(vp);
                    continue;
                }
                if(localName.equals("Gem")){
                    gems.putGem(gem);
                    continue;
                }
            }
        }















    }

    }
