package ua.nure.khachirov.SummaryTask3.Controllers;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;
import ua.nure.khachirov.SummaryTask3.entity.Gem;
import ua.nure.khachirov.SummaryTask3.entity.Gems;
import ua.nure.khachirov.SummaryTask3.entity.VisualParam;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;

public class DomController {
    private String xmlFileName;
    private Gems gems;
    public Gems getGemsFromDomController(){
        return gems;
    }

    public DomController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    public void parse() throws ParserConfigurationException, SAXException, IOException {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

        dbf.setNamespaceAware(true);

        dbf.setFeature(Constants.FEATURE_TURN_VALIDATION_ON, true);

        dbf.setFeature(Constants.FEATURE_TURN_SCHEMA_VALIDATION_ON, true);

        DocumentBuilder db = dbf.newDocumentBuilder();

        db.setErrorHandler(new DefaultHandler() {
            @Override
            public void error(SAXParseException e) throws SAXException {

                throw e;
            }
        });


        Document document = db.parse(xmlFileName);

        Element root = document.getDocumentElement();

        gems=new Gems();

        NodeList gemNodes = root.getElementsByTagName("Gem");
        for(int i=0;i<gemNodes.getLength();i++){
            Gem gem=getGem(gemNodes.item(i));
            gems.getGems().add(gem);
        }
    }

    public Gem getGem(Node gNode){
        Gem gem = new Gem();
        Element qElement = (Element) gNode;
       
        Node nameNode = qElement.getElementsByTagName("Name")
                .item(0);
        gem.setName(nameNode.getTextContent());
       
        Node preciousnessNode = qElement.getElementsByTagName("Preciousness")
                .item(0);
        gem.setPreciousness(Boolean.valueOf(preciousnessNode.getTextContent()));
        
        Node originNode = qElement.getElementsByTagName("Origin")
                .item(0);
        gem.setOrigin(originNode.getTextContent());
        
        Node valueNode = qElement.getElementsByTagName("Value")
                .item(0);
        gem.value=Integer.parseInt(valueNode.getTextContent());


        VisualParam vp=new VisualParam();
        Node visualParametersNode = qElement.getElementsByTagName("VisualParameters")
                .item(0);
        Element visualParametersElement = (Element) visualParametersNode;
        Node colorNode = visualParametersElement.getElementsByTagName("Color")
                .item(0);
        vp.setColor(colorNode.getTextContent());
        Node transparency = visualParametersElement.getElementsByTagName("Transparency")
                .item(0);
        vp.setTransparency(Integer.parseInt(transparency.getTextContent()));
        Node sposobOgranki = visualParametersElement.getElementsByTagName("Cuttingmethod")
                .item(0);
        vp.setCuttingmethod(Integer.parseInt(sposobOgranki.getTextContent()));
        gem.setVisualParam(vp);
       

        return gem;
    }
    public  Document getDocument(Gems gems) throws ParserConfigurationException {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

        dbf.setNamespaceAware(true);

        DocumentBuilder db = dbf.newDocumentBuilder();

        Document document = db.newDocument();


        Element tElement = document.createElement("Gems");
        document.appendChild(tElement);

        for(Gem g:gems.getGems()){
            Element gemElement = document.createElement("Gem");
            tElement.appendChild(gemElement);
            
            Element nameElement = document.createElement("Name");
            nameElement.setTextContent(g.getName());
            gemElement.appendChild(nameElement);
           
            Element preciousnessElement = document.createElement("Preciousness");
            preciousnessElement.setTextContent(String.valueOf(g.isPreciousness()));
            gemElement.appendChild(preciousnessElement);
            
            Element originElement = document.createElement("Origin");
            originElement.setTextContent(g.getOrigin());
            gemElement.appendChild(originElement);
          




            Element visualParamElement = document.createElement("VisualParameters");
            Element colorVisualParamElement = document.createElement("Color");
            colorVisualParamElement.setTextContent(g.getVisualParam().getColor());
            visualParamElement.appendChild(colorVisualParamElement);
           
            Element prozrachnostVisualParamElement = document.createElement("Transparency");
            prozrachnostVisualParamElement.setTextContent(String.valueOf(g.getVisualParam().getTransparency()));
            visualParamElement.appendChild(prozrachnostVisualParamElement);
           
            Element sposobOgrankiParamElement = document.createElement("Cuttingmethod");
            sposobOgrankiParamElement.setTextContent(String.valueOf(g.getVisualParam().getCuttingmethod()));
            visualParamElement.appendChild(sposobOgrankiParamElement);
            gemElement.appendChild(visualParamElement);
           
            Element valueElement = document.createElement("Value");
            valueElement.setTextContent(String.valueOf(g.value));
            gemElement.appendChild(valueElement);
           

        }
        return document;
    }
    public  void saveToXML(Gems gems, String xmlFileName)
            throws ParserConfigurationException, TransformerException {

        saveToXML(getDocument(gems), xmlFileName);
    }
    public  void saveToXML(Document document, String xmlFileName)
            throws TransformerException {

        StreamResult result = new StreamResult(new File(xmlFileName));

      
        TransformerFactory tf = TransformerFactory.newInstance();
        javax.xml.transform.Transformer t = tf.newTransformer();
        t.setOutputProperty(OutputKeys.INDENT, "yes");

        
        t.transform(new DOMSource(document), result);
    }
}
