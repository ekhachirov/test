package ua.nure.khachirov.SummaryTask3.Controllers;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import ua.nure.khachirov.SummaryTask3.entity.Gem;
import ua.nure.khachirov.SummaryTask3.entity.Gems;
import ua.nure.khachirov.SummaryTask3.entity.VisualParam;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.util.List;

public class SaxController extends DefaultHandler {

    private String xmlFileName;
    private String curElement;

    private Gems gems;
    public Gems getGems(){
        return gems;
    }

    private Gem gem;
    private VisualParam vp;



    public SaxController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }
    public void parse() throws ParserConfigurationException, SAXException, IOException {
        SAXParserFactory factory = SAXParserFactory.newInstance();
        factory.setNamespaceAware(true);
            factory.setFeature(Constants.FEATURE_TURN_VALIDATION_ON, true);
            factory.setFeature(Constants.FEATURE_TURN_SCHEMA_VALIDATION_ON, true);
        SAXParser parser = factory.newSAXParser();
        parser.parse(xmlFileName, this);
    }
    @Override
    public void error(org.xml.sax.SAXParseException e) throws SAXException {
        throw e;
    };



    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        curElement=localName;
        if(curElement.equals("Gems")){
            gems=new Gems();
            return;
        }
        if(curElement.equals("Gem")){
            gem=new Gem();
            return;
        }
        if(curElement.equals("VisualParameters")){
            vp=new VisualParam();
            return;
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
if(localName.equals("VisualParameters")){
    gem.setVisualParam(vp);
    return;
}
if(localName.equals("Gem")){
    gems.putGem(gem);
    return;
}

    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        String elementText = new String(ch, start, length).trim();
        if (elementText.isEmpty()) {
            return;
        }
        if(curElement.equals("Name")){
            gem.setName(elementText);
            return;
        }
        if(curElement.equals("Preciousness")){
            gem.setPreciousness(Boolean.valueOf(elementText));
            return;
        } if(curElement.equals("Origin")){
            gem.setOrigin(elementText);
            return;
        } if(curElement.equals("Color")){
            vp.setColor(elementText);
            return;
        }
        if(curElement.equals("Transparency")){
            vp.setTransparency(Integer.valueOf(elementText));
            return;
        }
        if(curElement.equals("Cuttingmethod")){
            vp.setCuttingmethod(Integer.valueOf(elementText));
            return;
        }
        if(curElement.equals("Value")){
            gem.value=Integer.valueOf(elementText);
            return;
        }

    }

    public List<Gem> getGemsList() {
        return gems.getGems();
    }








}
