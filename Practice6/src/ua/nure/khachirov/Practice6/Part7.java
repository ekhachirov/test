package ua.nure.khachirov.Practice6;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class Part7 implements Iterable<Integer> {

	private List<Integer> array = new ArrayList<>();

	private int n, m;

	private boolean reverse;

	public static void main(String[] args) {

		Part7 range = new Part7(3, 10, false);
		Iterator<Integer> iterator = range.iterator();
		while (iterator.hasNext()) {
			System.out.print(iterator.next() + " ");
		}
		Part7 range2 = new Part7(3, 10, true);
		Iterator<Integer> iterator2 = range2.iterator();
		System.out.println();
		while (iterator2.hasNext()) {
			System.out.print(iterator2.next() + " ");
		}
	}

	public Part7(int n, int m, boolean reverse) {
		this.n = n;
		this.m = m;
		this.reverse = reverse;
		for (int i = n; i <= m; i++) {
			array.add(i);
		}
		checkReverse();
	}

	private void checkReverse() {
		if (!this.reverse) {
			Collections.reverse(array);
		}
	}

	@Override

	public Iterator<Integer> iterator() {

		

		return new IteratorImpl();

	}

	private class IteratorImpl implements Iterator<Integer> {

		private int currentPos = -1;

		@Override

		public boolean hasNext() {
			if (currentPos + 1 == array.size()) {
				return false;
			}

			if (currentPos < array.size()) {
				return true;
			}

			return false;

		}

		@Override

		public Integer next() {
			return array.get(++currentPos);

		}

		@Override

		public void remove() {
		}

	}

	public int getN() {
		return n;
	}

	public void setN(int n) {
		this.n = n;
	}

	public int getM() {
		return m;
	}

	public void setM(int m) {
		this.m = m;
	}

}
