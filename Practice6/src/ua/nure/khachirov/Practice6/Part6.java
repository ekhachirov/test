package ua.nure.khachirov.Practice6;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import java.util.HashSet;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Part6 {
	public void work(){
		BufferedReader bReader=null;
		StringBuilder sBuilder=null;
		try {
			bReader = new BufferedReader(new InputStreamReader(System.in,"UTF-8"));
			 sBuilder=new StringBuilder(bReader.readLine());
		} catch (IOException e) {
			System.out.println("something wrong with reading from command line");
			}finally{
				try {
					bReader.close();
				} catch (IOException e) {
					System.out.println("cant close resource");
				}
			}
		String filePath=null;
		Pattern pattern=Pattern.compile("-i|--input ");
		Matcher matcher=pattern.matcher(sBuilder);
		if(matcher.find()){
			pattern=Pattern.compile("\\w+.txt");
			matcher=pattern.matcher(sBuilder);
			if(matcher.find()){
				filePath=matcher.group();
			}else {
				System.out.println("filePath have not been found");
				return;
			}
		}else {
			System.out.println("not proper input");
		}
		
		pattern=Pattern.compile("-t \\w+|--task \\w+");
		matcher=pattern.matcher(sBuilder);
		String taskName=null;
		if(matcher.find()){
			String[] temp=matcher.group().split(" ");
			taskName =temp[1];
		}else{
			System.out.println("Can not find task");
			System.exit(1);
		}
		
		switch(taskName){
		case "frequency":frequency(filePath);
		break;
		case "length":length(filePath);
		break;
		case "duplicates":duplicates(filePath);
		break;
		default :System.out.println("wrong task");
		return;
			
		}
		
		
		
	}
	private void frequency(String filePath){
		List<Word6> words=new ArrayList<Word6>();
		FileReader fileReader=null;
		StringBuilder stringBuilder=new StringBuilder();
		BufferedReader bufferedReader=null;
		try {
			 fileReader=new FileReader(filePath);
			  bufferedReader=new BufferedReader(fileReader);
			 String temp=null;
			 while((temp=bufferedReader.readLine())!=null){
				 stringBuilder.append(temp).append(System.lineSeparator());
			 }
			 stringBuilder.delete(stringBuilder.lastIndexOf(System.lineSeparator()),stringBuilder.length());
			 String[]wordsArray=stringBuilder.toString().split(" ");
			 for(int i=0;i<wordsArray.length;i++){
				 if(words.contains(new Word6(wordsArray[i]))){
					Word6 word= words.get(words.indexOf(new Word6(wordsArray[i])));
					word.setFrequency(word.getFrequency()+1);
				 }else{
					 words.add(new Word6(wordsArray[i],1));
				 }
			 }
			 Comparator<Word6> freqComp=new Comparator<Word6>() {
				
				@Override
				public int compare(Word6 o1, Word6 o2) {
					
					return o1.getFrequency()-o2.getFrequency();
				}
			};
			
			 Collections.sort(words,freqComp);
			 
			 List<Word6>finalList=words.subList( words.size()-3, words.size());
			 Comparator<Word6> alphabeticComp=new Comparator<Word6>() {

				@Override
				public int compare(Word6 o1, Word6 o2) {
					
					return -o1.getValue().compareTo(o2.getValue());
				}
			};
			 Collections.sort(finalList,alphabeticComp);
			 for(Word6 w:finalList){
				 System.out.println(w);
			 }
		}  catch (IOException e) {
			System.out.println("error");
		}finally{
			try {
				bufferedReader.close();
			} catch (IOException e) {
				System.out.println("cant close stream");
			}
		}
	}
	private void length(String filePath){
		Set<Word6>words=new HashSet<>();
		
		BufferedReader br=null;
		StringBuilder sb=new StringBuilder();
		try {
			br=new BufferedReader(new FileReader(filePath));
			String temp=null;
			while((temp=br.readLine())!=null){
				sb.append(temp).append(" ");
			}
			String []wordsArray=sb.toString().split(" ");
			for(int i=0;i<wordsArray.length;i++){
				words.add(new Word6(wordsArray[i]));
			}
			List<Word6>sorted=new ArrayList<>();
			for(Word6 w:words){
				sorted.add(w);
			}
			Comparator<Word6>comparator=new Comparator<Word6>() {

				@Override
				public int compare(Word6 o1, Word6 o2) {
					
					return -o1.getLength()-o2.getLength();
				}
			};
			Collections.sort(sorted,comparator);
			for(Word6 w:sorted.subList(0, 3)){
				System.out.println(w.getValue()+" ==> "+w.getLength());
				
			}
		} catch (IOException e) {
						System.out.println("error");
		}finally{
			try {
				br.close();
			} catch (IOException e) {
				System.out.println("cant close stream");
			}}
		
	}
	private void duplicates(String filePath){
		List<String>words=new LinkedList<>(); 
		BufferedReader br=null;
		StringBuilder sb=new StringBuilder();
	
		
			String temp=null;
			
			try {
				br=new BufferedReader(new FileReader(filePath));
				while((temp=br.readLine())!=null){
					sb.append(temp).append(" ");
				}
			} catch (IOException e) {
				System.out.println("errror");		
			
			}finally {
				try {
					br.close();
				} catch (IOException e) {
					System.out.println("cant close stream");
				}
			}
			String []wordsArray=sb.toString().split(" ");
			
			int mod=0;
			StringBuilder util;
			for(int i=0;i<wordsArray.length;i++){
				for (int j = i+1; j < wordsArray.length; j++) {
					if(wordsArray[i].equals(wordsArray[j])&mod!=3){
						util=new StringBuilder(wordsArray[i]);
						words.add(util.reverse().toString().toUpperCase());
						mod++;
					}
				}
			}
			for(String s:words){
				System.out.println(s);
			}
	
	}
public static void main(String []args){
	StringBuilder stringBuilder=new StringBuilder();
	if(args.length>0){
		for(int i=0;i<args.length;i++){
			stringBuilder.append(args[i]).append(" ");
		}
		try {
			System.setIn(new ByteArrayInputStream(stringBuilder.toString().getBytes("UTF-8")));
		} catch (UnsupportedEncodingException e) {
			System.out.println("error");
		}
	}
	Part6 p=new Part6();
	p.work();
	
}
}
