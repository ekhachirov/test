package ua.nure.khachirov.Practice6;

public class Word6 {
private String value;
private int frequency;
private int length;

@Override
public String toString() {
	// TODO Auto-generated method stub
	return value+" ==> "+frequency;
}
public Word6(String value, int frequency) {
	super();
	this.value = value;
	this.frequency = frequency;
	this.length=value.length();
}

public int getLength() {
	return length;
}
public void setLength(int length) {
	this.length = length;
}
@Override
public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((value == null) ? 0 : value.hashCode());
	return result;
}
@Override
public boolean equals(Object obj) {
	if (this == obj){
		return true;
	}
	if (obj == null){
		return false;
	}
	if (getClass() != obj.getClass()){
		return false;
	}
	Word6 other = (Word6) obj;
	if (value == null) {
		if (other.value != null){
			return false;
		}
	} else {
		if (!value.equals(other.value)){
		return false;
	}
	}
	return true;
}
public Word6(String value) {
	
	this.value = value;
	this.length=value.length();
	
}
public String getValue() {
	return value;
}
public void setValue(String value) {
	this.value = value;
}
public int getFrequency() {
	return frequency;
}
public void setFrequency(int frequency) {
	this.frequency = frequency;
}

}
