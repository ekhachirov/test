package ua.nure.khachirov.Practice6;

public class Word implements Comparable<Word>{
String value;
int freq=1;
public void incrFreq(){
	freq++;
}
public Word(String s){
	value=s;

}
@Override
public String toString() {
	return   value + ":" + freq ;
}
@Override
public int compareTo(Word arg0) {
	if(this.freq==arg0.freq){
		return this.value.compareTo(arg0.value);
	}else{
	return this.freq-arg0.freq;
}
}
@Override
public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((value == null) ? 0 : value.hashCode());
	return result;
}
@Override
public boolean equals(Object obj) {
	if (this == obj){
		return true;
		}
	if (obj == null){
		return false;
	}
	if (getClass() != obj.getClass()){
		return false;
	}
	Word other = (Word) obj;
	if (value == null) {
		if (other.value != null){
			return false;
		}
	} else{ if (!value.equals(other.value)){
		return false;
	}
	}
	return true;
}

}
