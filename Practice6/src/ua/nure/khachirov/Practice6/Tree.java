package ua.nure.khachirov.Practice6;

public class Tree<E extends Comparable<E>> {
	private Node<E> root = null;

	private static class Node<E extends Comparable<E>> {
		private E value;
		Node<E> parent;
		Node<E> left;
		Node<E> right;

		public Node(E value, Node<E> parent) {
			this.value = value;
			this.parent = parent;
		}

	}

	public boolean add(E[] elements) {

		for (E element : elements) {
			add(element);
		}
		return true;

	}

	public boolean add(E element) {
		if (root == null) {
			root = new Node<E>(element, null);
			return true;
		}

		Node<E> currentNode = root;
		Node<E> parent = null;

		int rez = 0;
		do {
			rez = currentNode.value.compareTo(element);
			if (rez < 0) {
				parent = currentNode;
				currentNode = currentNode.right;

			}
			if (rez > 0) {
				parent = currentNode;
				currentNode = currentNode.left;

			}
			if (rez == 0) {
				return false;
			}

		} while (currentNode != null);

		if (rez > 0) {
			parent.left = new Node<E>(element, parent);
		} else {
			parent.right = new Node<E>(element, parent);
		}
		return true;
	}

	public void print() {

		printWidth(root, 0);

	}

	public void delete(E element) {

		Node<E> curNode = root;
		while (element.compareTo(curNode.value) != 0) {
			if (curNode.value.compareTo(element) > 0) {
				if (curNode.left == null) {
					return;
				} else {
					curNode = curNode.left;
					continue;
				}

			}
			if (curNode.value.compareTo(element) < 0) {
				if (curNode.right == null) {
					return;
				} else {
					curNode = curNode.right;
				}
			}

		}

		if (curNode.right == null & curNode.left == null) {
			if (curNode.parent.left == curNode) {
				curNode.parent.left = null;
				return;
			}
			if (curNode.parent.right == curNode) {
				curNode.parent.right = null;
				return;
			}

		}
		if (curNode.right != null & curNode.left != null) {

			Node<E> min = findMin(curNode.right);
			if (curNode.right.left == null) {
				curNode.value = curNode.right.value;
				curNode.right = curNode.right.right;
			} else {
				curNode.value = min.value;
				if (min.parent.left.equals(min)) {
					min.parent.left = null;
				}

			}
		}
		if (curNode.right == null & curNode.left != null) {

			curNode.value = curNode.left.value;
			curNode.right = curNode.left.right;
			curNode.left = curNode.left.left;

		}
		if (curNode.left == null & curNode.right != null) {

			curNode.value = curNode.right.value;
			curNode.left = curNode.right.left;
			curNode.right = curNode.right.right;

		}

	}

	private Node<E> findMin(Node<E> start) {
		Node<E> cur = start;
		while (cur.left != null) {
			cur = cur.left;
		}

		return cur;
	}

	private void printWidth(Node<E> node, int depth) {

		if (node != null) {

			printWidth(node.right, depth + 1);

			for (int k = 0; k < depth; k++) {

				System.out.print("    ");

			}

			System.out.println(node.value);

			printWidth(node.left, depth + 1);

		}

	}
}
