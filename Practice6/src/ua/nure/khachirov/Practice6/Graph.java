package ua.nure.khachirov.Practice6;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Graph {
	private Map<Integer,List<Integer>> vertexes;
	public Graph(int numberOfVertex){
		vertexes=new HashMap<Integer,List<Integer>>();
		init(numberOfVertex);
	}
	private void init(int n){
		for(int i=1;i<n+1;i++){
			vertexes.put(i, new ArrayList<Integer>(n-1));
		}
	}
	public boolean addEdge(int name1,int name2){
		if(vertexes.containsKey(name1)){
			if(vertexes.get(name1).contains(name2)){
				return false;
			}
			if(name2<=vertexes.keySet().size()){
			vertexes.get(name1).add(name2);
			return true;
			}
		}
		return false;
	}
	public boolean deleteEdge(int name1,int name2  ){
		if(vertexes.containsKey(name1)){
			if(vertexes.get(name1).contains(name2)){
				vertexes.get(name1).remove(Integer.valueOf(name2));
				return true;
			}	
		}
		return false;
		
	}
	public Map<Integer,List<Integer>> getVertexes(){
		return vertexes;
	}
	public void printGraph(){
		for(int i: vertexes.keySet()){
		    System.out.println("vertex name:"+i+" "+Arrays.toString(vertexes.get(i).toArray()));
		}
	}
}
