package ua.nure.khachirov.Practice6;

import java.util.Arrays;
import java.util.Random;

public class Parking {
 private Car[]parking;
public Parking(int n){
	parking=new Car[n];
}
public Car[] getParking(){
	return parking;
}
public void getParkingState(){
	this.toString();
}
@Override
public String toString() {
	return "Parking [parking=" + Arrays.toString(parking) + "]";
}
public void carGoAway(){
	Random r =new Random();
	parking[r.nextInt(parking.length-1)]=null;
	
}
public boolean carComeIntoParking(Car c){
	Random r=new Random();
	int place=r.nextInt(parking.length-1);
	for(int i=place;i<parking.length;i++){
		if(parking[i]==null){
			parking[i]=c;
			return true;
		}else{
			continue;
		}
	
	}
	return false;
}
}
