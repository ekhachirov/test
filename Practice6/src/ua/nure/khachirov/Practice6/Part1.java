package ua.nure.khachirov.Practice6;

import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class Part1 {
	public static void main(String[] args)  {
		StringBuilder stringBuilder=new StringBuilder();
		if(args.length>0){
			for(int i=0;i<args.length;i++){
				stringBuilder.append(args[i]).append(" ");
			}
			try {
				System.setIn(new ByteArrayInputStream(stringBuilder.toString().getBytes("UTF-8")));
			} catch (UnsupportedEncodingException e) {
				System.out.println("error");
			}
		}
		List<Word> w = new ArrayList<>();
		WordContainer con = new WordContainer(w);
		Scanner sc = new Scanner(System.in,"UTF-8");
		while (sc.hasNext()) {
			String s = sc.nextLine();
			if (s.equals("stop")) {
				break;
			}

			String[] temp = s.split(" ");

			for (int i = 0; i < temp.length; i++) {
				con.add(new Word(temp[i]));
			}

		}

		Collections.sort(con.getConteiner(), Collections.reverseOrder());
		for (Word w1 : con.getConteiner()) {
			System.out.println(w1);
		}

		sc.close();
	}

}
