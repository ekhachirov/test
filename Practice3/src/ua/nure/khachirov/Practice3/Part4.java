package ua.nure.khachirov.Practice3;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Part4 {

	public static void main(String[] args) throws NoSuchAlgorithmException {
		try {
			System.out.println(hash("password", "SHA-256"));
			System.out.println(hash("passwort", "SHA-256"));
		} catch (UnsupportedEncodingException e) {
			System.out.println("cant hash password");
		}
		

	}

	public static String hash(String input, String algorithm) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		StringBuilder sbrez = new StringBuilder();
		MessageDigest digest = MessageDigest.getInstance(algorithm);
		digest.update(input.getBytes("UTF-8"));
		byte[] hash = digest.digest();

		for (int i = 0; i < hash.length; i++) {
			String tempBinaryShapeOfByte = Integer.toBinaryString(hash[i]);
			StringBuilder binaryShapeOfByte = new StringBuilder();
			int numberOfAddedZero;
			if (tempBinaryShapeOfByte.length() < 8) {
				numberOfAddedZero = 8 - tempBinaryShapeOfByte.length();
				for (int j = 0; j < numberOfAddedZero; j++) {
					binaryShapeOfByte.append("0");
				}
				binaryShapeOfByte.append(tempBinaryShapeOfByte);
			} else {
				binaryShapeOfByte.append(tempBinaryShapeOfByte.substring(
						tempBinaryShapeOfByte.length() - 8, tempBinaryShapeOfByte.length()));
			}
			int[] n = new int[2];
			Pattern p = Pattern.compile("\\d{4}");
			Matcher m = p.matcher(binaryShapeOfByte);
			int c = 0;
			while (m.find()) {
				n[c++] = Integer.parseInt(m.group(), 2);
			}
			for (int forech : n) {
				switch (forech) {
				case 10:
					sbrez.append("A");
					break;
				case 11:
					sbrez.append("B");
					break;
				case 12:
					sbrez.append("C");
					break;
				case 13:
					sbrez.append("D");
					break;
				case 14:
					sbrez.append("E");
					break;
				case 15:
					sbrez.append("F");
					break;
				default:
					sbrez.append(forech);
				}
			}
		}

		return sbrez.toString();

	}
}
