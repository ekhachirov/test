package ua.nure.khachirov.Practice3;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Part5 {
	public static void main(String[] args) {

		System.out.println(roman2Decimal("LXXV"));
		System.out.println(decimal2Roman(30));
	}

	public static int roman2Decimal(String s) {

		int rez = 0;
		Pattern p = Pattern.compile("X[LC]|I[VX]");
		Matcher m = p.matcher(s);
		while (m.find()) {
			switch (m.group()) {
			case "IV":
				rez += 4;
				break;
			case "IX":
				rez += 9;
				break;
			case "XL":
				rez += 40;
				break;
			case "XC":
				rez += 90;
				break;
			}
		}
		String withoutDoubleCharacter = s.replaceAll("X[LC]|I[VX]", " ");
		Pattern p2 = Pattern.compile("\\w");
		Matcher m2 = p2.matcher(withoutDoubleCharacter);
		while (m2.find()) {
			switch (m2.group()) {
			case "I":
				rez += 1;
				break;
			case "V":
				rez += 5;
				break;
			case "X":
				rez += 10;
				break;
			case "L":
				rez += 50;
				break;
			case "C":
				rez += 100;
				break;
			}
		}
		return rez;
	}

	public static String decimal2Roman(int inputNumber) {
		StringBuilder rez = new StringBuilder();
		int repeat;
		int number=inputNumber;
		int arabicSymbols[] = { 1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1 };
		String romanSymbol[] = { "M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I" };
		repeat = number / 1;
		for (int x = 0; 0 < number; x++) {
			repeat = number / arabicSymbols[x];
			for (int i = 1; i <= repeat; i++) {
				rez.append(romanSymbol[x]);
			}
			number = number % arabicSymbols[x];
		}
		return rez.toString(); 
	}

}
