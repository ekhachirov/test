package ua.nure.khachirov.Practice5;

import java.io.IOException;
import java.io.RandomAccessFile;

public class ThreadPart6 extends Thread {
    RandomAccessFile ra;
    
    char message;
    public ThreadPart6(RandomAccessFile ra, char mes){
        this.ra=ra;
        message=mes;
       
    }
    
    @Override
    public void run() {
        synchronized (ra) {
            try {
                  if(ra.getFilePointer()!=0){
                	  ra.write(System.lineSeparator().getBytes("UTF-8"));
                  }
            	for(int i=0;i<20;i++){
            		
            		ra.write(message);
            		
            		
            	}
                
               

            } catch (IOException e) {
            	System.out.println("something wrong in threadpart6");
            } 
        }
    }

}
