package ua.nure.khachirov.Practice5;

public class NotSyncThread extends Thread {
	Counter counter;

	public NotSyncThread(Counter c) {
		counter = c;
	}

	public void run() {

		System.out.println(counter.getCounter1() == counter.getCounter2());
		counter.incCounter1();

		try {
			sleep(10);
		} catch (InterruptedException e) {
			System.out.println("something wrong in notsyncThred");
		}
		counter.incCounter2();

	}

}
