package ua.nure.khachirov.Practice5;

public class Part3 {

	public static void main(String[] args) throws InterruptedException {
		Counter c = new Counter();

		NotSyncThread t1 = new NotSyncThread(c);
		NotSyncThread t2 = new NotSyncThread(c);
		NotSyncThread t3 = new NotSyncThread(c);

		t1.start();
		Thread.sleep(1);
		t2.start();
		Thread.sleep(1);
		t3.start();

		t1.join();
		t2.join();
		t3.join();

		System.out.println(c.getCounter1() + "==>" + c.getCounter2());
		c.setCounter1(0);
		c.setCounter2(0);
		Thread[] t = new Thread[3];
		for (int i = 0; i < 3; i++) {
			t[i] = new SyncThread(c);
			t[i].start();
			t[i].join();
		}

		System.out.println(c.getCounter1() + "==>" + c.getCounter2());

	}

}
