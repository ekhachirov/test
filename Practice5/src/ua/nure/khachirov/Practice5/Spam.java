package ua.nure.khachirov.Practice5;

import java.io.IOException;

public class Spam {
	static final int[] TIME = { 1000, 4000, 8000 };

	static final String[] TEXT = { "A", "B", "C" };

	public static void main(String[] args) throws InterruptedException {
		Spam.main(TIME, TEXT);

	}

	public static Thread main(int[] time, String[] text) throws InterruptedException {
		final Thread[] t = new Thread[time.length];
		for (int i = 0; i < time.length; i++) {
			t[i] = new MyThread(time[i], text[i]);
		}
		for (Thread tEach : t) {
			tEach.start();
		}

		Thread thread = new Thread() {
			public void run() {
				byte[] buffer = new byte[10];
				int count;
				try {
					do {
						while ((count = System.in.read(buffer)) == -1){};
					} while (!System.lineSeparator().equals(new String(buffer, 0, count,"UTF-8")));
				} catch (IOException ex) {
					System.out.println("something wrong in spam");
				}
				for (Thread tEach : t) {
					tEach.interrupt();
				}
			}
		};
		thread.start();

		return thread;

	}
}
