package ua.nure.khachirov.Practice5;

import java.util.Arrays;

public class MatrixThread extends Thread {
	int[][] matrix;
	int row;
	int rez;

	public MatrixThread(int[][] matrix, int row) {
		this.matrix = Arrays.copyOf(matrix, matrix.length);
		this.row = row;

	}

	public void run() {
		int max = matrix[row][0];
		for (int i = 0; i < matrix[row].length; i++) {
			if (max < matrix[row][i]) {
				try {
					sleep(1);
				} catch (InterruptedException e) {
				
					System.out.println("something wrong in matrixthread");
				}
				max = matrix[row][i];
			}
		}
		rez = max;
	}

}
