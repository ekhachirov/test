package ua.nure.khachirov.Practice5;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Part51 {
	private static final int ITERATION_NUMBER = 3;

	private static final int READERS_NUMBER = 3;

	private static final StringBuilder BUFFER = new StringBuilder();

	private static final int BUFFER_LENGTH = 5;

	private static final int PAUSE = 5;

	private static boolean stop;

	private static boolean exit;

	private static class Reader extends Thread {

		public void run() {

			while (!stop) {

				try {

					synchronized (BUFFER) {

						if (exit) {

							return;

						}

						BUFFER.wait();

						read(getName());

						sleep(10);

					}

				} catch (InterruptedException e) {

					e.getMessage();

				}

			}

		}

	}

	private static class Writer extends Thread {

		public void run() {

			int tact = 0;

			while (!stop) {

				try {

					synchronized (BUFFER) {

						if (tact == ITERATION_NUMBER - 1) {

							exit = true;

						}

						write();

						sleep(100);

						BUFFER.notifyAll();

					}

					sleep(100);

				} catch (InterruptedException e) {

					e.getMessage();

				} finally {

					if (++tact == ITERATION_NUMBER) {

						stop = true;

					}

				}

			}

		}

	}

	public static void main(String[] args) throws InterruptedException {

		Writer writer = new Writer();

		List<Thread> readers = new ArrayList<>();

		for (int j = 0; j < READERS_NUMBER; j++) {

			readers.add(new Reader());

		}

		for (Thread reader : readers) {

			reader.setDaemon(true);

		}

		Thread.sleep(10);

		for (Thread reader : readers) {

			reader.start();

		}

		writer.start();

		Thread.sleep(10);

		writer.join();

		for (Thread reader : readers) {

			reader.join();

		}

	}

	private synchronized static void write() throws InterruptedException {

		BUFFER.setLength(0);

		System.err.print("Writer writes:");

		Random random = new Random();

		for (int j = 0; j < BUFFER_LENGTH; j++) {

			Thread.sleep(PAUSE);

			char ch = (char) ('A' + random.nextInt(26));

			System.err.print(ch);

			BUFFER.append(ch);

		}

		System.err.println();

		Thread.sleep(5);

	}

	private synchronized static void read(String threadName) throws InterruptedException {

		System.out.printf("Reader %s:", threadName);

		for (int j = 0; j < BUFFER_LENGTH; j++) {

			Thread.sleep(PAUSE);

			System.out.print(BUFFER.charAt(j));

		}

		System.out.println();

		Thread.sleep(5);

	}

}
