package ua.nure.khachirov.Practice5;

public class Counter {

	private int counter1 = 0;
	private int counter2 = 0;

	public int getCounter1() {
		return counter1;
	}

	public int getCounter2() {
		return counter2;
	}

	public void setCounter1(int c1) {
		counter1 = c1;
	}

	public void setCounter2(int c2) {
		counter2 = c2;
	}

	public void incCounter1() {
		counter1++;
	}

	public void incCounter2() {
		counter2++;
	}
}
