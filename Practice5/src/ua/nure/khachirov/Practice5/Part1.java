package ua.nure.khachirov.Practice5;

public class Part1 {

	static Thread tT = new Thread() {

		public void run() {
			setName("ByExtending");
			int count = 0;

			while (count <= 4) {
				System.out.println(getName());
				count++;
				try {
					sleep(500);
				} catch (InterruptedException e) {
					return;
				}
			}

		}
	};
	static Runnable n = new Runnable() {

		@Override
		public void run() {

			Thread.currentThread().setName("ByImpl");
			int count = 0;
			while (count <= 4) {
				System.out.println(Thread.currentThread().getName());
				count++;
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					return;
				}

			}

		}
	};

	static Thread tR = new Thread(n);

	public static void main(String[] args) throws InterruptedException {
		tR.start();
		tT.start();

	}

}
