package ua.nure.khachirov.Practice5;

import java.util.ArrayList;

import java.util.Date;

import java.util.List;

import java.util.Random;

import java.util.concurrent.*;

import java.util.concurrent.locks.Condition;

import java.util.concurrent.locks.Lock;

import java.util.concurrent.locks.ReentrantLock;

public class Part52 {

	private static final int ITERATION_NUMBER = 3;

	private static final int READERS_NUMBER = 3;

	private static final StringBuilder BUFFER = new StringBuilder();

	private static final int BUFFER_LENGTH = 5;

	private static final int PAUSE = 5;

	private static boolean stop;

	private final static Lock LOCK = new ReentrantLock();

	private final static Condition CONDITION = LOCK.newCondition();

	private static boolean exit;

	private static class Reader extends Thread implements Condition {

		public void run() {

			while (!stop) {

				try {

					LOCK.lock();

					try {

						if (exit) {

							return;

						}
						CONDITION.await();

						read(getName());

						sleep(10);

					} finally {

						LOCK.unlock();

					}

				} catch (InterruptedException e) {

					e.getMessage();

				}

			}

		}

		@Override

		public void await() throws InterruptedException {

			// TODO Auto-generated method stub

		}

		@Override

		public void awaitUninterruptibly() {

			// TODO Auto-generated method stub

		}

		@Override

		public long awaitNanos(long nanosTimeout) throws InterruptedException {

			// TODO Auto-generated method stub

			return 0;

		}

		@Override

		public boolean await(long time, TimeUnit unit) throws InterruptedException {

			// TODO Auto-generated method stub

			return false;

		}

		@Override

		public boolean awaitUntil(Date deadline) throws InterruptedException {

			// TODO Auto-generated method stub

			return false;

		}

		@Override

		public void signal() {

			// TODO Auto-generated method stub

		}

		@Override

		public void signalAll() {

			// TODO Auto-generated method stub

		}

	}

	private static class Writer extends Thread {

		public void run() {

			int tact = 0;

			while (!stop) {

				try {

					LOCK.lock();

					try {

						if (tact == ITERATION_NUMBER - 1) {

							exit = true;

						}

						write();

						sleep(100);

						CONDITION.signalAll();

					} finally {

						LOCK.unlock();

					}

					sleep(100);

				} catch (InterruptedException e) {

					e.getMessage();

				} finally {

					if (++tact == ITERATION_NUMBER) {

						stop = true;

					}

				}

			}

		}

	}

	private synchronized static void read(String threadName) throws InterruptedException {

		System.out.printf("Reader %s:", threadName);

		for (int j = 0; j < BUFFER_LENGTH; j++) {

			Thread.sleep(PAUSE);

			System.out.print(BUFFER.charAt(j));

		}

		Thread.sleep(5);

		System.out.println();

	}

	private synchronized static void write() throws InterruptedException {

		System.err.print("Writer writes:");

		BUFFER.setLength(0);

		Random random = new Random();

		for (int j = 0; j < BUFFER_LENGTH; j++) {

			Thread.sleep(PAUSE);

			char ch = (char) ('A' + random.nextInt(26));

			System.err.print(ch);

			BUFFER.append(ch);

		}

		Thread.sleep(5);

		System.err.println();

	}

	public static void main(String[] args) throws InterruptedException {

		List<Thread> readers = new ArrayList<>();

		for (int j = 0; j < READERS_NUMBER; j++) {

			readers.add(new Reader());

		}

		

		Writer writer = new Writer();

		Thread.sleep(10);

		for (Thread reader : readers) {

			reader.start();

		}

		writer.start();

		Thread.sleep(10);

		writer.join();

		for (Thread reader : readers) {

			reader.join();

		}

		

	}

}
