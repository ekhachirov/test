package ua.nure.khachirov.Practice5;

public class MyThread extends Thread {
	  private String text;
	 private int interval;

	public MyThread(int interval, String text) {
		this.interval = interval;
		this.text = text;
	}

	public void run() {
		while (!this.isInterrupted()) {
			try {
				sleep(interval);
				System.out.print(text);
			} catch (InterruptedException e) {

				break;
			}
		}

	}

}
