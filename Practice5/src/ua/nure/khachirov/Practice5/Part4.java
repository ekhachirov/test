package ua.nure.khachirov.Practice5;

import java.util.Arrays;
import java.util.Random;

public class Part4 {
	int[][] matrix;
	Thread[] t;
	int[] rezultsOfThreads;

	public Part4(int[][] matrix) {
		this.matrix = Arrays.copyOf(matrix, matrix.length);
		rezultsOfThreads = new int[matrix.length];
	}

	public int culcMaxValueMultiThreading() throws InterruptedException {
		t = new Thread[matrix.length];
		for (int i = 0; i < matrix.length; i++) {
			t[i] = new MatrixThread(matrix, i);
			t[i].start();
			t[i].join();
		}

		for (int i = 0; i < rezultsOfThreads.length; i++) {
			rezultsOfThreads[i] = ((MatrixThread) t[i]).rez;

		}
		Arrays.sort(rezultsOfThreads);

		return rezultsOfThreads[rezultsOfThreads.length - 1];

	}
	public int maxByBruteForce(){
		int max=matrix[0][0];
		for(int i=0;i<matrix.length;i++){
			for(int j=0;j<matrix[i].length;j++){
				try {
					Thread.sleep(1);
				} catch (InterruptedException e) {
					System.out.println("something wrong in part4");
				}
				if(matrix[i][j]>max){
					max=matrix[i][j];
				}
			}
		}
		return max;
		
	}
	public static void main(String[]args) throws InterruptedException{
		Random r=new Random();
		int [][]m=new int[4][100];
		for(int i=0;i<m.length;i++){
			for(int j=0;j<m[0].length;j++){
				m[i][j]=r.nextInt(1000);
			}
		}
		Part4 p=new Part4(m);
		long before=System.currentTimeMillis();
		System.out.println("Max element==>"+p.culcMaxValueMultiThreading());
		long after=System.currentTimeMillis();
		System.out.println("ByUsingThreading"+" : "+(after-before));
		before=System.currentTimeMillis();
		System.out.println("Max element==>"+p.maxByBruteForce());
		after=System.currentTimeMillis();
		System.out.println("Simple bruteForce"+" : "+(after-before));
	}
	
}
