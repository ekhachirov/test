package ua.nure.khachirov.Practice5;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;



public class Part2 {

private	static final int[] TIME = { 1000, 4000, 8000 };

	private static final String[] TEXT = { "A", "B", "C" };

	public static void main(String[] args) throws InterruptedException, UnsupportedEncodingException {

		InputStream stdIn = System.in;

		ByteArrayInputStream bais = new ByteArrayInputStream(System.lineSeparator().getBytes("UTF-8"));

		bais.skip(System.lineSeparator().length());

		System.setIn(bais);

		Thread thread = Spam.main(TIME, TEXT);

		Thread.sleep(5000);

		bais.reset();

		thread.join();

		System.setIn(stdIn);

	}

}
