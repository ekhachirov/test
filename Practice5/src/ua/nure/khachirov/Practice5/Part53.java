package ua.nure.khachirov.Practice5;

import java.util.ArrayList;

import java.util.List;

import java.util.Random;


import java.util.concurrent.locks.Lock;

import java.util.concurrent.locks.ReentrantLock;

public class Part53 {

	private static final int ITERATION_NUMBER = 3;

	private static final int READERS_NUMBER = 3;

	private static final StringBuilder BUFFER = new StringBuilder();

	private static final int BUFFER_LENGTH = 5;

	private static final int PAUSE = 5;

	private static boolean stop;

	private final static Lock LOCK = new ReentrantLock();

	private static boolean exit;

	private static class Reader extends Thread {

		public void run() {

			while (!stop) {

				try {

					synchronized (LOCK) {

						if (exit) {

							return;

						}

						LOCK.wait();

						read(getName());

						sleep(10);

					}

				} catch (InterruptedException e) {

					e.getMessage();

				}

			}

		}

	}

	private static class Writer extends Thread {

		public void run() {

			int tact = 0;

			while (!stop) {

				try {

					synchronized (LOCK) {

						if (tact == ITERATION_NUMBER - 1) {

							exit = true;

						}

						write();

						LOCK.notifyAll();

					}

					sleep(200);

				} catch (InterruptedException eX) {

					eX.getMessage();

				} finally {

					if (++tact == ITERATION_NUMBER) {

						stop = true;

					}

				}

			}

		}

	}

	private synchronized static void read(String threadName) throws InterruptedException {

		System.out.printf("Reader %s:", threadName);

		for (int j = 0; j < BUFFER_LENGTH; j++) {

			System.out.print(BUFFER.charAt(j));

			Thread.sleep(PAUSE);

		}

		Thread.sleep(5);

		System.out.println();

	}

	private synchronized static void write() throws InterruptedException {

		System.err.print("Writer writes:");

		BUFFER.setLength(0);

		Random rand = new Random();

		for (int i = 0; i < BUFFER_LENGTH; i++) {

			Thread.sleep(PAUSE);

			char ch = (char) ('A' + rand.nextInt(26));

			System.err.print(ch);

			BUFFER.append(ch);

		}

		Thread.sleep(10);

		System.err.println();

	}

	public static void main(String[] args) throws InterruptedException {

		List<Thread> readers = new ArrayList<>();

		for (int j = 0; j < READERS_NUMBER; j++) {

			readers.add(new Reader());

		}

		Writer writer = new Writer();

		Thread.sleep(10);

		for (Thread reader : readers) {

			reader.start();

		}

		writer.start();

		Thread.sleep(10);

		writer.join();

		for (Thread reader : readers) {

			reader.join();

		}

	}

}